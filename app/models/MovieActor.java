package models;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Set;

@Entity
@Table(name = "MOVIE_ACTORS_ENTITY_GRAPH")
public class MovieActor implements Serializable {
    @Id
    @Column(name = "ID")
    private Integer id;

    @NotNull
    @Size(max = 50)
    @Column(name = "ACTOR")
    private String actor;

    @ManyToOne
    @JoinColumn(foreignKey = @ForeignKey(name = "MOVIE_ACTORS_GRAPH_FK"), name = "MOVIE_ID", referencedColumnName = "ID")
    private Movie movie;

    @OneToMany
    @JoinColumn(name = "ACTOR_ID", referencedColumnName = "ID")
    private Set<MovieActorAward> movieActorAwards;

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        MovieActor that = (MovieActor) o;

        return id.equals(that.id);
    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }

    public String getActor() {
      return this.actor;
    }

    public Set<MovieActorAward> getMovieActorAwards() {
      return this.movieActorAwards;
    }
}
