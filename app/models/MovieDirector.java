package models;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Set;

@Entity
@Table(name = "MOVIE_DIRECTORS_ENTITY_GRAPH")
public class MovieDirector {
    @Id
    @Column(name = "ID")
    private Integer id;

    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "DIRECTOR")
    private String director;

    @ManyToOne
    @JoinColumn(foreignKey = @ForeignKey(name = "MOVIE_DIRECTORS_GRAPH_FK"), name = "MOVIE_ID", referencedColumnName = "ID")
    private Movie movie;

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        MovieDirector that = (MovieDirector) o;

        return id.equals(that.id);
    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }
}
