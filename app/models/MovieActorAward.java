package models;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Set;

@Entity
@Table(name = "MOVIE_ACTOR_AWARDS_ENTITY_GRAPH")
public class MovieActorAward {
    @Id
    @Column(name = "ID")
    private Integer id;

    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "AWARD")
    private String award;

    @ManyToOne
    @JoinColumn(foreignKey = @ForeignKey(name = "MOVIE_ACTOR_AWARDS_GRAPH_FK"), name = "ACTOR_ID", referencedColumnName = "ID")
    private MovieActor movieActor;

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;

        MovieActorAward that = (MovieActorAward) o;

        return id.equals(that.id);
    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }
}
