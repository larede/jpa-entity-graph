package services;

import javax.persistence.EntityGraph;
import java.util.List;

import play.db.jpa.JPA;

import models.Movie;

public class MovieService {

    public static List<Movie> listMovies() {
        return JPA.em().createNamedQuery("Movie.findAll")
            .getResultList();
    }

    public static List<Movie> listMovies(String hint, String graphName) {
        return JPA.em().createNamedQuery("Movie.findAll")
            .setHint(hint, JPA.em().getEntityGraph(graphName))
            .getResultList();
    }

    public static List<Movie> listMovies(String hint, EntityGraph<?> entityGraph) {
        return JPA.em().createNamedQuery("Movie.findAll")
            .setHint(hint, entityGraph)
            .getResultList();
    }

    public static List<Movie> listMoviesById(Integer movieId, String hint, String graphName) {
        return JPA.em().createNamedQuery("Movie.findAllById")
            .setParameter("movieId", movieId)
            .setHint(hint, JPA.em().getEntityGraph(graphName))
            .getResultList();
    }

    public static List<Movie> listMoviesByIds(List<Integer> movieIds, String hint, String graphName) {
        return JPA.em().createNamedQuery("Movie.findAllByIds")
            .setParameter("movieIds", movieIds)
            .setHint(hint, JPA.em().getEntityGraph(graphName))
            .getResultList();
    }
}
