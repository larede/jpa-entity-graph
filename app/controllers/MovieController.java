package controllers;

import play.mvc.*;
import services.MovieService;
import play.db.jpa.Transactional;
import play.libs.Json;

public class MovieController extends Controller {

    @Transactional
    public Result listMovies() {
      return ok(Json.toJson(MovieService.listMovies("EAGER", "movieWithActorsAndAwards")));
    }
}
